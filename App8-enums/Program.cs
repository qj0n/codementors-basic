﻿using System;

namespace Codementors.BasicCsharp.App8_enums
{
    class Program
    {
        static MathOperation GetOperation(char oper)
        {
            switch (oper)
            {
                case '+':
                    return MathOperation.Addition;
                case '-':
                    return MathOperation.Subtracttion;
                case '/':
                    return MathOperation.Division;
                case '*':
                case 'x':
                    return MathOperation.Multiplication;
                case '^':
                    return MathOperation.Power;
                case '%':
                    return MathOperation.Modulo;
                default:
                    return MathOperation.Unknown;
            }
        }

        static void Main(string[] args)
        {
            Console.Write("Podaj pierwszą liczbę: ");
            int first = App4_sumator.Program.ReadInt();

            Console.Write("Podaj drugą liczbę: ");
            int second = App4_sumator.Program.ReadInt();

            Console.Write("Podaj znak operacji: ");
            char operation = Console.ReadKey().KeyChar;

            int result;
            switch(GetOperation(operation))
            {
                case MathOperation.Addition:
                    result = first + second;
                    break;
                case MathOperation.Subtracttion:
                    result = first - second;
                    break;
                case MathOperation.Multiplication:
                    result = first * second;
                    break;
                case MathOperation.Division:
                    result = first / second;
                    break;
                case MathOperation.Power:
                    result = (int)Math.Pow(first, second);
                    break;
                case MathOperation.Modulo:
                    result = first % second;
                    break;
                default:
                case MathOperation.Unknown:
                    Console.Error.WriteLine("Nieznana operacja");
                    return;
            }

            Console.WriteLine();
            Console.WriteLine($"{first} {operation} {second} = {result}");
        }
    }
}

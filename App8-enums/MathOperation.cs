﻿namespace Codementors.BasicCsharp.App8_enums
{
    public enum MathOperation
    {
        /// <summary>
        /// + operation, sum
        /// </summary>
        Addition,

        Subtracttion,
        Multiplication,
        Division,
        Power,
        Modulo,
        Unknown,
    }
}

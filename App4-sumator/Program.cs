﻿using System;

namespace Codementors.BasicCsharp.App4_sumator
{
    class Program
    {
        /// <summary>
        /// Read integer from standard input
        /// </summary>
        /// <returns><see cref="int"/> read</returns>
        static int ReadInt()
        {
            // read string
            // 'var' means autotype - here var = string
            var readString = Console.ReadLine();

            // here var = int
            var readValue = Convert.ToInt32(readString);

            return readValue;
            
            // we could put it in one line:
            // return Convert.ToInt32(Console.ReadLine());
        }

        /// <summary>
        /// Main code of application
        /// </summary>
        /// <remarks>This is run on the start and application will close, when it will hit the end</remarks>
        /// <param name="args">command line parameters - not used here</param>
        static void Main(string[] args)
        {
            Console.Write("Podaj pierwszą liczbę: ");
            int first = ReadInt();

            Console.Write("Podaj drugą liczbę: ");
            int second = ReadInt();

            Console.Write("Podaj znak operacji (+ lub -): ");
            char operation = Console.ReadKey().KeyChar;

            int result;
            if (operation == '+')
                result = first + second;
            else
            {
                if (operation == '-')
                    result = first - second;
                else
                {
                    Console.Error.WriteLine("Nieznana operacja");
                    return;
                }
            }

            Console.WriteLine($"{first} {operation} {second} = {result}");
        }
    }
}

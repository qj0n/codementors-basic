﻿using System;
namespace Codementors.BasicCsharp.App3
{
    class Program
    {
        /// <summary>
        /// Prints our variables on standard output with <see cref="Console"/>
        /// </summary>
        /// <param name="a">integer variable</param>
        /// <param name="c">double variable</param>
        static void PrintVariables(int a, double c)
        {
            Console.WriteLine(@"integer = {a}");
            Console.WriteLine($"real = {c}");

            Console.WriteLine($"number + real = {a + c}");
        }

        /// <summary>
        /// Main code of application
        /// </summary>
        /// <remarks>This is run on the start and application will close, when it will hit the end</remarks>
        /// <param name="args">command line parameters - not used here</param>
        static void Main(string[] args) 
        {
            // define variables (zmienne)
            int number = 42;
            double real = 3.141592653589793;

            // print variables
            PrintVariables(number, real);

            // read new values
            // TODO zaimlpementować ReadValues
            ReadValues(number, real);

            // print variables
            PrintVariables(number, real);
        }
    }
}

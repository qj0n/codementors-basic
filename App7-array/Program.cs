﻿using System;

namespace Codementors.BasicCsharp.App7_array
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Ile liczb chcesz zsumować: ");
            int count = App4_sumator.Program.ReadInt();

            // initialize int array
            int[] data = new int[count];

            //read data
            for(int i=0; i<count; i++)
            {
                // read array element
                Console.Write($"Podaj {i}-tą liczbę: ");
                data[i] = App4_sumator.Program.ReadInt();
            }

            int sum = 0;
            foreach (int number in data)
            {
                // short for sum = sum + number
                sum += number;
            }

            // $"{data[0]} + {data[1]} + ... + {data[count-1]}"
            string expression = string.Join(" + ", data);

            Console.WriteLine();
            Console.WriteLine($"{expression} = {sum}");
        }
    }
}

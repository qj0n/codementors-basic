﻿using System;
using System.Collections.Generic;
using Codementors.BasicCsharp.App8_enums;

namespace Codementors.BasicCsharp.App9_dictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Podaj pierwszą liczbę: ");
            int first = App4_sumator.Program.ReadInt();

            Console.Write("Podaj drugą liczbę: ");
            int second = App4_sumator.Program.ReadInt();

            Console.Write("Podaj znak operacji: ");
            char operation = Console.ReadKey().KeyChar;

            /// dictionary maps chars to enum values
            Dictionary<char, MathOperation> charToEnum = new Dictionary<char, MathOperation>
            {
                ['+'] = MathOperation.Addition,
                ['-'] = MathOperation.Subtracttion,
                ['*'] = MathOperation.Multiplication,
                ['x'] = MathOperation.Multiplication,
                ['/'] = MathOperation.Division,

                ['^'] = MathOperation.Power,
                ['%'] = MathOperation.Modulo,
            };

            int result;
            switch (charToEnum[operation])
            {
                case MathOperation.Addition:
                    result = first + second;
                    break;
                case MathOperation.Subtracttion:
                    result = first - second;
                    break;
                case MathOperation.Multiplication:
                    result = first * second;
                    break;
                case MathOperation.Division:
                    result = first / second;
                    break;
                case MathOperation.Power:
                    result = (int)Math.Pow(first, second);
                    break;
                case MathOperation.Modulo:
                    result = first % second;
                    break;
                default:
                case MathOperation.Unknown:
                    Console.Error.WriteLine("Nieznana operacja");
                    return;
            }

            Console.WriteLine();
            Console.WriteLine($"{first} {operation} {second} = {result}");
        }
    }
}
